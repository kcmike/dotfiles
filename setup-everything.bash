#!/bin/bash
shopt -s dotglob
rln() {
	# $1 = relative path
	if [ -f ./"$1" ] ; then
		ln -iv ./"$1" ~/"$1"
	elif [ -d ./"$1" ] ; then
		mkdir -pv ~/"$1"
		for f in ./"$1"/* ; do
			rln $(echo "$f" | sed -E 's@^./@@')
		done
	fi
}
for i in .[a-z]* ; do
	rln "$i"
done
