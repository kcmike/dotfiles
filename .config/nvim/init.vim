set tabstop=4
set shiftwidth=4
set scrolloff=8
filetype indent off
map ` a<C-R>=strftime('%FT%T') <CR>
