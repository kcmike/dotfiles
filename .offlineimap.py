from subprocess import check_output

def get_pass(site):
	return check_output('gpg -dq ~/.offlineimappass.gpg | fgrep %s | cut -f 2 -d " "' % site,
		shell=True).strip('\n')
